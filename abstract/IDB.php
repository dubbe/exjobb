<?php
interface IDB{
	/*
		__construct should call function connect, 
		this will set private prop $db to a 
		connected database on instantiation.
	*/

	public function Connect();
	public function Close();
	public function Prepare($slq);
	public function RunQuery($stmt);
	public function FetchObjects($stmt, $class, $nameOfPrimareKey);
}
?>