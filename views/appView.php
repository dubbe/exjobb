<?php
class appView{
	public function __construct(){
		
	}

	public function renderBaseHtml($mainContent = '', $isTestView = true){
		$html = "<!DOCTYPE html>
						<html lang='en'>
						  <head>
						    <meta charset='utf-8'>
						    <title>Testmaster 5000</title>
						    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
						    <meta name='description' content=''>
						    <meta name='author' content=''>

				    		<script type='text/javascript' src='http://code.jquery.com/jquery-1.7.2.min.js'></script>

						    <link href='/assets/css/bootstrap.css' rel='stylesheet'/>
						    <style>
						      body {
						        padding-top: 60px;
						      }
						    </style>
						    <link href='/assets/css/bootstrap-responsive.css' rel='stylesheet'/>

						    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
						    <!--[if lt IE 9]>
						      <script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
						    <![endif]-->
						  </head>

						  <body>

						    <div class='navbar navbar-fixed-top'>
						      <div class='navbar-inner'>
						        <div class='container'>
						          <a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
						            <span class='icon-bar'></span>
						            <span class='icon-bar'></span>
						          </a>
						          <a class='brand' href='/'>Testmaster 5000</a>
						          <div class='nav-collapse'>
						            <ul class='nav'>";
						            if($isTestView){
					            		$html .= "<li class='active'><a href='/'>Test</a></li>
					            		<li><a href='/result'>Results</a></li>";
						            }else{
					            		$html .= "<li><a href='/'>Test</a></li>
					            		<li class='active'><a href='/result'>Results</a></li>";
						            }
						              
						           $html .= "</ul>
						          </div><!--/.nav-collapse -->
						        </div>
						      </div>
						    </div>

						    <div class='container'>
							  <div>
						      	$mainContent
						      </div>

						    </div> 

						    
						    <script type='text/javascript' src='/assets/js/bootstrap-transition.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-alert.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-modal.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-dropdown.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-scrollspy.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-tab.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-tooltip.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-popover.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-button.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-collapse.js'></script>
						    <script type='text/javascript' rc='/assets/js/bootstrap-carousel.js'></script>
						    <script type='text/javascript' src='/assets/js/bootstrap-typeahead.js'></script>

						  </body>
						</html>";

						return $html;
	}

	public function renderTestForm(){
		return "<div class='hero-unit'>
		    		<h1>Testmaster 5000</h1>
		    			<p>Make your DB sweat...</p>
		    	</div>
				<div class='ajax'>
					<img src='assets/img/ajax.gif'/>
				</div>
				<form class='form-horizontal' action='' method='POST'>
			        <fieldset>
			          <legend>Enter Testdata</legend>

			          <div class='control-group'>
			            <label class='control-label' for='database'>Database</label>
			            <div class='controls'>
			              <select id='database' name='database'>
			                <option>azure</option>
			                <option>couchdb</option>
			                <option>couchdbLocal</option>
			                <option>databaseDotCom</option>
			              </select>
			            </div>
			          </div>

			          <div class='control-group'>
			            <label class='control-label' for='action'>Action</label>
			            <div class='controls'>
			              <select id='action' name='action'>
			                <option>Create</option>
			                <option>Read</option>
			                <option>Update</option>
			                <option>Delete</option>
			              </select>
			            </div>
			          </div>

			          <div class='control-group'>
			            <label class='control-label' for='amount'>Amount</label>
			            <div class='controls'>
			              <select id='amount' name='amount'>
			                <option>1</option>
			                <option>100</option>
			                <option>1000</option>
			                <option>10000</option>
			                <option>100000</option>
			              </select>
			            </div>
			          </div>
						
					  <div class='form-actions'>
			            <button type='submit' class='btn btn-primary' id='submittest'>Run Test</button>
			          </div>

			        </fieldset>
			      </form>

			      

			      <style>
						.ajax { display: none; width: 200px; text-align: center; margin: 0 auto;}
			      </style>

			      <script type='text/javascript'>
			      	$('#submittest').click(function(){
			      		$('.ajax').css('display', 'block');
			      	});
			      </script>";
	}

	public function renderLatestTestResultTable($result){
		return "<div class='alert alert-success'>
    				<p>Success!</p>
    			</div>
    		<h1>Results from latest testrun</h1>
				<table class='table table-striped table-bordered table-condensed'>
					<tr>
						<th>Date</th>
						<th>Database</th>
						<th>Action</th>
						<th>Amount</th>
						<th>Time/Seconds</th>
					</tr>
					".$this->generateTabelRows($result)."
				</table>
				<div>
					<a href='' class='btn'>New Test</a>
				</div>";
	}

	public function renderAllResultsTable($resultsArray){
		$table = "<h1>Filter Results</h1>
				<form class='form-inline' action='/result' method='POST' style='width: 100%;'>

					<div class='control-group' style='display: block; float: left; margin-right: 5px;'>
        				<label class='control-label' for='dbselect'>Filter Databases</label>
			            <div class='controls'>
			              <select multiple='multiple' id='dbselect' name='dbselect[]'>
			                <option value='azure'>azure</option>
			                <option value='couchdb'>couchdb cloud</option>
			                <option value='couchdbLocal'>couchdb local</option>
			                <option value='databaseDotCom'>databaseDotCom</option>
			              </select>
			            </div>
			          </div>

			          <div class='control-group' style='display: block; float: left; margin-right: 5px;'>
        				<label class='control-label' for='actionselect'>Filter Actions</label>
			            <div class='controls'>
			              <select multiple='multiple' id='actionselect' name='actionselect[]'>
			                <option value='Create'>Create</option>
			                <option value='Read'>Read</option>
			                <option value='Update'>Update</option>
			                <option value='Delete'>Delete</option>
			              </select>
			            </div>
			          </div>
			          <div>

			          <div class='control-group' style='display: block; float: left; margin-right: 5px;'>
        				<label class='control-label' for='amoutselect'>Filter Amount</label>
			            <div class='controls'>
			              <select multiple='multiple' id='amoutselect' name='amoutselect[]'>
			                <option value='1'>1</option>
			                <option value='100'>100</option>
			                <option value='1000'>1000</option>
			                <option value='10000'>10000</option>
			                <option value='100000'>100000</option>
			              </select>
			            </div>
			          </div>

			          <div class='control-group' style='display: block; float: left; margin-right: 5px;'>
			            <label class='control-label' for='orderby'>Order By</label>
			            <div class='controls'>
			              <select id='orderby' name='orderby'>
			                <option>Database</option>
			                <option>Action</option>
			                <option>Amount</option>
			                <option>Time</option>
			                <option>Time Desc</option>
			              </select>
			            </div>
			          </div>

			          <div style='clear: both;'></div>

			          <div style='margin-bottom: 10px;'>
						<button type='submit' class='btn btn-primary' id='submittest'>Filter results</button>
			          </div>
		          </form>

		          <h1>Results from all testruns</h1>
				<table class='table table-striped table-bordered table-condensed'>
					<tr>
						<th>Date</th>
						<th>Database</th>
						<th>Action</th>
						<th>Amount</th>
						<th>Time</th>
					</tr>";
		foreach ($resultsArray as $result) {
			$table .= $this->generateTabelRows($result);
		}
		$table .= "
				</table>";

		return $table;
	}

	public function generateTabelRows($result){
		return "<tr>
						<td>" . $result->Date . "</td>
						<td>" . $result->Database_name . "</td>
						<td>" . $result->Operation_name . "</td>
						<td>" . $result->Amount . "</td>
						<td>" . $result->Time . "</td>
					</tr>";
	}
}

?>