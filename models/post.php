<?php
class post{
	public $id;
	public $author;
	public $title;
	public $content;
	public $created_date ;

	/* For couchDB */
	public $comments = array() ;

	public static function create($content = null){
		$post = new post();

		if($content == null) {
			$post->author = "Test";
			$post->title = "Lorem ipsum";
			$post->content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
			$post->created_date = date('Y-m-d\TH:i:s');
		} else {
			if($content->_id != null) {
				$post->id = $content->_id ;
				$post->_id = $content->_id ;
			}
			if($content->_rev != null) {
				$post->_rev = $content->_rev ;
			}
			$post->author = $content->author ;
			$post->title = $content->title ;
			$post->content = $content->content ;
			$post->created_date = $content->created_date ;
			$post->comments = $content->comments ;
		}

		return $post;
	}

	public function addComment($comment) {
		if(is_array($comment)) {
			foreach($comment as $c) {
				array_push($this->comments, $c) ;
			}
		} else {
			array_push($this->comments, $comment) ;
		}
	}

}
?>