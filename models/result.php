<?php
class result{
	public $TestID;
	public $Date;
	public $Time;
	public $Database_name;
	public $Operation_name;
	public $Amount;


	public static function create($_date, $_time, $_database_name, $_operation_name, $_amount){
		$r = new result();
		
		$r->Date = $_date;
		$r->Time = $_time;
		$r->Database_name = $_database_name;
		$r->Operation_name = $_operation_name;
		$r->Amount = $_amount;

		return $r;
	}
}
?>