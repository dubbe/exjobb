<?php

require_once('models/post.php') ;
require_once('models/comment.php') ;
require_once('models/result.php');

class testController{
	//Database members.
	private $m_db;
	private $m_resultDB;

	//Result object props.
	const CREATE = "Create";
	const READ = "Read";
	const UPDATE = "Update";
	const DELETE = "Delete";
	
	private $databaseName;
	private $Start_time;
	private $End_time;
	private $Date;

	function __construct($resultDB, $_db){

		require_once('database/'.$_db.'.php');

		$this->m_db = new $_db ;
		$this->m_resultDB = $resultDB;

		//Props for Result object.
		$this->databaseName = $_db;
		$this->Date = date('Y-m-d\TH:i:s');
	}

	/*
	* Function that will run test in specified DB and create and store an Result obect
	*/
	public function initTest($action, $amount){
		//Sets amount to 1000 if $amount doesnt have value or if $amount is unvalid.
		$amount = $this->Set_amount($amount);
		//Sets private prop $this->start, to a millisecond float value.
		$this->Set_start_time();

		$this->m_db->$action($amount);
		
		//Sets private prop $this->end, to a millisecond float value.
		$this->Set_end_time();

		//Create an instance of a Resultobject and stores it in ResultDB.
		$result = $this->Create_result($action, $amount);

		//Close DBs
		$this->m_db->close();
		$this->m_resultDB->CloseConnection();
		return $result;
	}

	function Create_result($action, $amount){
		//Second and millisecond value of the time elapsed while the test was running.
		$time = $this->End_time - $this->Start_time;
		//Static method in result object.
		$result = result::create($this->Date, $time, $this->databaseName, $action, $amount);
		//Insertion into the ResultDB database.
		$this->m_resultDB->createResult($result);
		return $result;
	}
	//Validation of amount, returns 1000 if not valid.
	function Set_amount($amount){
		if($amount == '' || !is_numeric($amount)) {
			return 1000 ;
		}
		return $amount;
	}

	function Set_start_time(){
		$this->Start_time = $this->Get_microtime_float();
	}
	
	function Set_end_time(){
		$this->End_time = $this->Get_microtime_float();
	}

	function Get_microtime_float()
	{
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}
}
?>