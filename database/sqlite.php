<?php 
//Read DB interface
require_once('database/pdobase.php');

//Alll DBs must fullfill IDB interface
class sqlite extends pdobase{

	public function __construct(){
		$this->Connect();
		$this->Create_Tables();
	}

	public function Connect(){
		try{
			
			$this->db = new PDO('sqlite:http://vps.dubbe.se/exjobb_sqlite/TestData.sqlite');
			$this->db->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e){
			die('ERROR OCCURED with testSQLiteDB! ->' .$e->getMessage());
		}
	}

	public function Create_Tables(){
		$query = 'CREATE TABLE IF NOT EXISTS Post
		 (
 			id INTEGER PRIMARY KEY AUTOINCREMENT,
 			author TEXT,
 			content TEXT,
			title TEXT,
			created_date TEXT
		 );';
		$sth = $this->Prepare($query);

		if(!$sth->execute()){
			die("Error when creating table");
		}

		$query = 'CREATE TABLE IF NOT EXISTS Comment
		 (
 			id INTEGER PRIMARY KEY AUTOINCREMENT,
 			postId INTEGER,
 			author TEXT,
 			content TEXT,
			title TEXT,
			created_date TEXT
		 );';
		$sth = $this->Prepare($query);

		if(!$sth->execute()){
			die("Error when creating table");
		}
	}

	public function createObject($model){
		if(strtolower(get_class($model)) == "post"){
			$query = "INSERT INTO Post (author, title, content, created_date) VALUES(:author, :title, :content, :created_date)";
			$sth = $this->db->Prepare($query);
		}
		else{
			$query = "INSERT INTO Comment (postId, author, title, content, created_date) VALUES(:postId, :author, :title, :content, :created_date)";
			
			$sth = $this->db->Prepare($query);

			$sth->bindParam(':postId', $model->postId, PDO::PARAM_STR, 50);
		}
	
		$sth->bindParam(':author', $model->author, PDO::PARAM_STR, 50);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR, 50);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR, 50);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR, 50);

		if ($sth === FALSE){
			return false;
		}

		$this->ExecuteQuery($sth);
		return $this->db->lastInsertId();
	}

	public function updatePost($model){

		$query = "UPDATE Post SET author = :author, title = :title, content = :content, created_date = :created_date WHERE id = ".$model->id;
		$sth = $this->db->Prepare($query);
	
		$sth->bindParam(':author', $model->author, PDO::PARAM_STR, 50);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR, 50);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR, 50);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR, 50);


		if ($sth === FALSE){
			return false;
		}

		$this->ExecuteQuery($sth);
		return $this->db->lastInsertId();
	}

	public function deleteObject($object, $tableName){
		$query = "DELETE FROM ".$tableName." WHERE id = :id";

		$sth = $this->db->prepare($query);

		$sth -> bindParam(":id", $object->id, PDO::PARAM_INT);

		if ($sth === false) {
			return false;
		}

		$this->ExecuteQuery($sth);
	}

	public function readRow($id, $tableName){
		if($tableName === "comment"){
			$query = "SELECT * FROM ".$tableName." WHERE postId = :id";
		}else{
			$query = "SELECT * FROM ".$tableName." LIMIT 1 OFFSET :id";
		}

		$sth = $this->db->Prepare($query);
		$id = (int)$id;

		$sth->bindParam(':id', $id, PDO::PARAM_INT);

		if ($sth === FALSE) {
			return false;
		}

		$sth = $this->ExecuteQuery($sth);
		return $this->FetchObjects($sth, strtolower($tableName));
	}
	
}
?>