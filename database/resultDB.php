<?php

require_once('database/pdobase.php');

class resultDB extends pdobase{

	const SERVER = "pfq10irlyg.database.windows.net";
	const USERNAME = "menda@pfq10irlyg";
	const PASSWORD = "1Br@Lösen=rd?";
	const DBNAME = "azuretest";
	const TESTTABLENAME = "testResult";

	private $database = NULL;

	public function __construct(){
		$this->connect();
	}
	
	public function Connect(){
		try{
			$this->database = new PDO("sqlsrv:server=".resultDB::SERVER."; Database = ".resultDB::DBNAME, resultDB::USERNAME, resultDB::PASSWORD);
			$this->database->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e){
			die(printf($e)) ;
		}
	}
	
	public function CloseConnection(){
		$this->database = NULL;
	}

	function readResults(){	
		$query = "EXECUTE Read_result";

		$sth = $this->database->Prepare($query);

		
		if(!$sth->execute()){
			die("Error when reading resultstable");
		}

		$arr = array();

		while($object = $sth->fetchObject('result')){
			$arr[] = $object;
		}

		return $arr;
	}

	function CreateResult($result){
		$query = "EXECUTE Create_result 
						@Date = :Date, @Time = :Time, @Database_name = :Database_name, @Operation_name = :Operation_name, @Amount = :Amount;";
		
		$sth = $this->database->Prepare($query);

		$time = strval($result->Time);

		$sth->bindParam(':Date', $result->Date, PDO::PARAM_STR);
		$sth->bindParam(':Time', $time, PDO::PARAM_STR);
		$sth->bindParam(':Database_name', $result->Database_name, PDO::PARAM_STR);
		$sth->bindParam(':Operation_name', $result->Operation_name, PDO::PARAM_STR);
		$sth->bindParam(':Amount', $result->Amount, PDO::PARAM_INT);

		if(!$sth->execute()){
			die("Error when creating table");
		}
	}

	public function readFilteredResults($dbFilterArray, $actionFilterArray, $amountArray, $orderBy){
		$query = 'SELECT * FROM '.resultDB::TESTTABLENAME;
		$operator = " WHERE ";
		
		switch ($orderBy) {
			case 'Database':
				$orderBy = "(Database_name)";
				break;
			case 'Action':
				$orderBy = "(Operation_name)";
				break;
			case 'Amount':
				$orderBy = "(Amount)";
				break;
			case 'Time':
				$orderBy = "(Time)";
				break;
			case 'Time Desc':
				$orderBy = "(Time) DESC";
				break;
			default:
				$orderBy = "(Database_name)";
				break;
		}
		
		//DB filter
		if (count($dbFilterArray) > 0) {
			$dbWhere = $operator .= "(Database_name = ";

			for ($i=0; $i < count($dbFilterArray); $i++) { 
				$dbWhere .= "'".$dbFilterArray[$i]."'";
				if ($i !== count($dbFilterArray) -1) {
					$dbWhere .= ' OR Database_name = ';
				}
			}

			$query .= $dbWhere . ')';
			$operator = " AND "; 
		}

		//Action Filter
		if (count($actionFilterArray) > 0) {
			$actionWhere = $operator .= "(Operation_name = ";
			
			for ($j=0; $j < count($actionFilterArray); $j++) { 
				$actionWhere .= "'".$actionFilterArray[$j]."'";
				if ($j !== count($actionFilterArray) -1) {
					$actionWhere .= ' OR Operation_name = ';
				}
			}

			$query .= $actionWhere . ')';
			$operator = " AND "; 
		}

		//Amount Filter
		if (count($amountArray) > 0) {
			$amountWhere = $operator .= "(Amount = ";
			
			for ($j=0; $j < count($amountArray); $j++) { 
				$amountWhere .= $amountArray[$j];
				if ($j !== count($amountArray) -1) {
					$amountWhere .= ' OR Amount = ';
				}
			}

			$query .= $amountWhere . ')'; 
		}


		$query .= " ORDER BY ".$orderBy;
		
		$sth = $this->database->Prepare($query);

		
		if(!$sth->execute()){
			die("Could not filter result!");
		}

		$arr = array();

		while($object = $sth->fetchObject('result')){
			$arr[] = $object;
		}

		return $arr;
	}
}