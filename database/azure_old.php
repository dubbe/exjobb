<?php 

require_once('database/pdobase.php');


//Alll DBs must fullfill IDB interface
class azure extends pdobase{

	
	const SERVER = "pfq10irlyg.database.windows.net";
	const USERNAME = "menda@pfq10irlyg";
	const PASSWORD = "1Br@Lösen=rd?";
	const DBNAME = "azuretest";

	public function __construct(){
		$this->connect();
	}

	public function Connect(){
		try{
			$this->db = new PDO("sqlsrv:server=".azure::SERVER."; Database = ".azure::DBNAME, azure::USERNAME, azure::PASSWORD);
			$this->db->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e){
			die(printf($e)) ;
		}
	}

	public function createObject($model){
		if(strtolower(get_class($model)) == "post"){
			$query = "INSERT INTO Post (author, title, content, created_date) VALUES (:author, :title, :content, :created_date);";
			$sth = $this->db->Prepare($query);
		}
		else{
			$query = "INSERT INTO Comment (postId, author, title, content, created_date) VALUES (:postId, :author, :title, :content, :created_date);";
			
			$sth = $this->db->Prepare($query);
			$sth->bindParam(':postId', $model->postId, PDO::PARAM_STR);
		}

		$sth->bindParam(':author', $model->author, PDO::PARAM_STR);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR);

		
		$this->ExecuteQuery($sth);
		return (int)$this->db->lastInsertId();
	}

	public function updatePost($model){

		$query = "UPDATE Post SET author = :author, title = :title, content = :content, created_date = :created_date WHERE id = :id";
		$sth = $this->db->Prepare($query);
	
		$sth->bindParam(':author', $model->author, PDO::PARAM_STR);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR);
		$sth -> bindParam(":id", $object->id, PDO::PARAM_INT);

		if ($sth === FALSE){
			return false;
		}

		$this->ExecuteQuery($sth);
		return $this->db->lastInsertId();
	}

	public function deleteObject($object, $tableName){
		$query = "DELETE FROM ".$tableName." WHERE id = :id";

		$sth = $this->db->prepare($query);

		$sth -> bindParam(":id", $object->id, PDO::PARAM_INT);

		if ($sth === false) {
			return false;
		}

		$this->ExecuteQuery($sth);
	}

	public function readRow($id, $tableName){
		$id = (int)$id;

		if($tableName === "comment"){
			$query = "SELECT * FROM ".$tableName." WHERE postId = :id";
		}else{
			//$query = "SELECT TOP 1 * FROM ".$tableName;
			$query = "WITH PostAR AS (SELECT ROW_NUMBER() OVER(ORDER BY id) AS RowNum,* FROM Post) SELECT * FROM PostAR WHERE RowNum = :id";
		}
		$sth = $this->db->Prepare($query);
		$sth->bindParam(':id', $id, PDO::PARAM_INT);

		if ($sth === FALSE) {
			return false;
		}

		$sth = $this->ExecuteQuery($sth);
		return $this->FetchObjects($sth, strtolower($tableName));
	}
	
}
?>