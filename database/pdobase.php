<?php 

require_once('database/dbBase.php');

class pdobase extends dbBase {

	public $db;

	public function Close(){
		$this->db = NULL;
	}

	public function Prepare($query){
		try {
			$sth = $this->db->prepare($query);
		} catch (Exception $e) {
			die(printf($e));
		}
		return $sth;
	}

	public function ExecuteQuery($sth){
		try {
			$sth->execute();
		} catch (Exception $e) {
			die(printf($e));
		}
		return $sth;
	}


	public function FetchObjects($sth, $className){
		$arr = array();

		while($object = $sth->fetchObject($className)){
			$arr[] = $object;
		};
		
		return $arr;
	}
}
?>