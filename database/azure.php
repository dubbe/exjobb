<?php 

require_once('database/pdobase.php');


//Alll DBs must fullfill IDB interface
class azure extends pdobase{

	
	const SERVER = "pfq10irlyg.database.windows.net";
	const USERNAME = "menda@pfq10irlyg";
	const PASSWORD = "1Br@Lösen=rd?";
	const DBNAME = "azuretest";

	public function __construct(){
		$this->connect();
	}

	public function Connect(){
		try{
			$this->db = new PDO("sqlsrv:server=".azure::SERVER."; Database = ".azure::DBNAME, azure::USERNAME, azure::PASSWORD);
			$this->db->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(Exception $e){
			die(printf($e)) ;
		}
	}


	public function createObject($model){
		if(strtolower(get_class($model)) == "post"){
			$query = "EXECUTE Create_post 
						@author = :author, @title = :title, @content = :content, @created_date = :created_date;";
			$sth = $this->db->Prepare($query);
		}
		else{
			$query = "EXECUTE Create_comment 
						@postId = :postId, @author = :author, @title = :title, @content = :content, @created_date = :created_date;";
			
			$sth = $this->db->Prepare($query);
			$sth->bindParam(':postId', $model->postId, PDO::PARAM_STR);
		}

		$sth->bindParam(':author', $model->author, PDO::PARAM_STR);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR);

		
		$this->ExecuteQuery($sth);
		return (int)$this->db->lastInsertId();
	}


	public function updatePost($model){

		$query = "EXECUTE Update_post 
						@author = :author, @title = :title, @content = :content, @created_date = :created_date, @id = :id;";
		$sth = $this->db->Prepare($query);
	
		$sth->bindParam(':author', $model->author, PDO::PARAM_STR);
		$sth->bindParam(':title', $model->title, PDO::PARAM_STR);
		$sth->bindParam(':content', $model->content, PDO::PARAM_STR);
		$sth->bindParam(':created_date', $model->created_date, PDO::PARAM_STR);
		$sth->bindParam(":id", $object->id, PDO::PARAM_INT);

		if ($sth === FALSE){
			return false;
		}

		$this->ExecuteQuery($sth);
		return $this->db->lastInsertId();
	}

	public function deleteObject($object, $tableName){
		$query = "EXECUTE Delete_".strtolower($tableName)." @id = :id;";

		$sth = $this->db->prepare($query);

		$sth -> bindParam(":id", $object->id, PDO::PARAM_INT);

		if ($sth === false) {
			return false;
		}

		$this->ExecuteQuery($sth);
	}

	public function readRow($iterator, $tableName){
		$tableName = strtolower($tableName);
		$id = (int)$iterator;
		$query = "EXECUTE Read_".$tableName." @id = :id";

		$sth = $this->db->Prepare($query);
		$sth->bindParam(':id', $iterator, PDO::PARAM_INT);

		if ($sth === FALSE) {
			return false;
		}

		$sth = $this->ExecuteQuery($sth);
		return $this->FetchObjects($sth, $tableName);
	}
}