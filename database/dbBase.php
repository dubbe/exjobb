<?php
class dbBase{
	public function create($amount){
		for ($i=0; $i < $amount; $i++) {
			$post = Post::create();
			$postId = $this->createObject($post);
			//Create 5 Comments for each post.
			for ($j=0; $j < 2; $j++) {
				$comment = Comment::create($postId);
				$this->createObject($comment);
			}
		}
	}

	public function read($amount){
		for ($i=1; $i < $amount +1; $i++) {
			$post = $this->readRow($i, "Post");
			$post = $post[0];
			$comment = $this->readRow($post->id, "Comment");
			$post->addComment($comment);
		}
		return;
	}

	public function update($amount){
		for ($i=1; $i < $amount +1; $i++) { 
			$post = $this->readRow($i, "Post");
			$post = $post[0];
			$post->content = "Nullam eu ligula et augue mattis mollis. Donec vestibulum blandit ante, varius sagittis velit dapibus in. Nullam congue mattis est non eleifend. Sed quis lorem velit. Fusce sit amet nunc nec ante porttitor ultricies quis et neque. Nulla elementum dui in quam accumsan malesuada. Suspendisse potenti. Nam ultrices, eros in laoreet posuere, dui arcu consectetur felis, non varius leo dolor eget erat. Mauris ut adipiscing orci. Ut ullamcorper aliquam est, imperdiet auctor ligula aliquam vel. Ut varius dolor et nisl semper id elementum sem placerat. Etiam erat nulla, interdum vel mattis nec, ultricies eu dolor. Aliquam erat volutpat. Vivamus iaculis luctus sapien vel pellentesque. ";
			$this->updatePost($post);
		}
		return;
	}

	public function delete($amount){
		for ($i=1; $i < $amount +1; $i++) { 
			$post = $this->readRow($i, "Post");
			$post = $post[0];
			$comments = $this->readRow($post->id, "Comment");
			foreach ($comments as $comment) {
				$this->deleteObject($comment, "Comment");
			}
			$this->deleteObject($post, "Post");
		}
		return;
	}
}
?>