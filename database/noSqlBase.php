<?php

require_once('models/comment.php') ;
require_once('models/post.php') ;

class noSqlBase {

    private $domain = 'http://exjobb.iriscouch.com'; #Your Domain
    private $port = '5984'; #Default Port
    private $database = 'exjobb'; #Database
    private $username = ''; #Username
    private $password = ''; #Password

    private $lastKey = null ;

    public function __construct() {
    }

    public function setUp() {
        $this->make_call('exjobb','DELETE');
        $this->make_call('exjobb', 'PUT') ;
    }

    public function create($amount) {

        for($i=0;$i<$amount;$i++) {

            $id = uniqid() ;
            
            $post = $this->toJson(Post::create()) ;

            $response = $this->make_call($id, 'PUT', $post) ;
            
        }

    }

    public function read($amount) {

        for($i=0;$i<$amount;$i++) {

            if($this->lastKey == null) {
                $request = $this->make_call('_design/order/_view/date?limit=1&skip=1&include_docs=true&descending=true', 'GET') ;
            } else {
                $request = $this->make_call('_design/order/_view/date?limit=1&startkey="'.$this->lastKey.'"&skip=1&include_docs=true&descending=true', 'GET') ;
            }  

            $this->lastKey = $request->rows[0]->id ;
        }
    }



    public function update($amount) {

        $posts = $this->read($amount) ;

        foreach($posts as $p) {
            $p = Post::create($p->doc) ;

            $p->content = "Nullam eu ligula et augue mattis mollis. Donec vestibulum blandit ante, varius sagittis velit dapibus in. Nullam congue mattis est non eleifend. Sed quis lorem velit. Fusce sit amet nunc nec ante porttitor ultricies quis et neque. Nulla elementum dui in quam accumsan malesuada. Suspendisse potenti. Nam ultrices, eros in laoreet posuere, dui arcu consectetur felis, non varius leo dolor eget erat. Mauris ut adipiscing orci. Ut ullamcorper aliquam est, imperdiet auctor ligula aliquam vel. Ut varius dolor et nisl semper id elementum sem placerat. Etiam erat nulla, interdum vel mattis nec, ultricies eu dolor. Aliquam erat volutpat. Vivamus iaculis luctus sapien vel pellentesque. ";
            $response = $this->make_call($p->_id, 'PUT', $this->toJson($p)) ;
        }

    }

    public function delete($id, $tableName = null) {
        return $this->make_call($id, 'DELETE') ;
    }

    // Dummy function, no use in nosql
    public function connect() {
    }

    // Dummy function, no use in nosql
    public function close() {

    }

    protected function make_call($m_url, $m_method, $m_message = null) {

        $t_url = $this->domain.':'.$this->port.'/'.$this->database.'/'.$m_url;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $t_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $m_method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $m_message);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8')); 

        $result = curl_exec($ch);

        $result = json_decode($result, false);   

         if(isset($result->_id) || isset($result->id) || isset($result->rows) ) {

         } else {
            echo "<pre>" ;
            var_dump($t_url) ;
            var_dump($result) ;
            echo "</pre>" ;      
        }
        
        return $result;
    }

    // class name - Mf_Data
    // exlcuded properties - $_parent, $_index
    public function toJson($model){
        $array = get_object_vars($model);
        unset($array['_parent'], $array['_index']);
        array_walk_recursive($array, function(&$property, $key){
            if(is_object($property)
            && method_exists($property, 'toArray')){
                $property = $property->toArray();
            }
        });
        return json_encode($array);
    }

}