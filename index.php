<?php
    date_default_timezone_set('Europe/Stockholm') ;
    set_time_limit(10000); 
    error_reporting(-1); ini_set('display_errors', true);
    require_once("views/appView.php");
    require_once("database/resultDB.php");
    require_once("models/result.php");
    require_once('models/post.php');
    require_once('models/postDBDC.php');
    require_once('models/comment.php');

        
    $view = new appView();
    $resultDB = new resultDB();

    $request = explode("/", substr(@$_SERVER['REQUEST_URI'], 1));

    if(isset($_POST['database'])){
        require_once("controllers/testController.php");
        $testDBName = $_POST['database'];
        $actionName = $_POST['action'];
        $amount = $_POST['amount'];
        $params['amount'] = $_POST['amount'];

        $controller = new testController($resultDB, $testDBName) ;

        //$result = call_user_func_array(array($controller, $actionName), $params);
        $result = $controller->initTest($actionName, $amount);

        $table = $view->renderLatestTestResultTable($result);
        $html = $view->renderBaseHtml($table);
    }
    elseif (isset($_POST['dbselect']) || isset($_POST['actionselect']) || isset($_POST['amoutselect']) || isset($_POST['orderby'])) {
        $dbFilterArray = array();
        $actionFilterArray = array();
        $amountArray = array();
        $orderBy = "";

        if (isset($_POST['dbselect'])) {
            $dbFilterArray = $_POST['dbselect'];
        }
        if (isset($_POST['actionselect'])) {
            $actionFilterArray = $_POST['actionselect'];
        }
        if (isset($_POST['amoutselect'])) {
            $amountArray = $_POST['amoutselect'];
        }
        if (isset($_POST['orderby'])) {
            $orderBy = $_POST['orderby'];
        }
        
        $table = $view->renderAllResultsTable($resultDB->readFilteredResults($dbFilterArray, $actionFilterArray, $amountArray, $orderBy));
        $html = $view->renderBaseHtml($table, false);
    }
    elseif ($request[0] === "result") {
        $table = $view->renderAllResultsTable($resultDB->readResults());
        $html = $view->renderBaseHtml($table, false);
    }
    else{

        $form = $view -> renderTestForm();
        $html = $view -> renderBaseHtml($form);
    }
    echo($html);
?>
